// Slovička
const words = ['javascript', 'canvas', 'svg', 'eventlistener', 'html', 'css', 'kaj'];

// Náhodné slovo pro hru
let randomWord = '';

// Počet pokusů (čítač)
let attempts = 0;

// Počet pismen ve slově
let countOfLetters = 0;

// Počet špatných pokusů (čítač)
let wrongAttempts = 0;

// Pismena, která hráč uhadnul
let guessedLetters = [];

// Přístup k prvkům stránky
const countOfLettersElement = document.getElementById('count-of-letters');
const attemptsElement = document.getElementById('attempts');
const wrongAttemptsElement = document.getElementById('wrong-attempts');
const guessedLettersElement = document.getElementById('guessed-letters');
const wordDisplayElement = document.getElementById('word-display');
const guessInputElement = document.getElementById('guess-input');
const guessButtonElement = document.getElementById('guess-button');
const hangmanImageElement = document.getElementById('hangman-image');
const restartButtonElement = document.getElementById('restart-button');

// Funkce pro volbu nahodného slova
function chooseRandomWord() {
  randomWord = words[Math.floor(Math.random() * words.length)];
  countOfLetters = randomWord.length;
  countOfLettersElement.textContent = countOfLetters;
}

// Funkce pro aktualizaci vykreslení na stránce
function updateGameDisplay() {
  attemptsElement.textContent = attempts;
  wrongAttemptsElement.textContent = wrongAttempts;
  guessedLettersElement.textContent = guessedLetters.join(' ');

  let displayWord = '';
  for (let letter of randomWord) {
    if (guessedLetters.includes(letter)) {
      displayWord += letter;
    } else {
      displayWord += '_';
    }
  }
  wordDisplayElement.textContent = displayWord;
}

// Funkce pro aktualizaci obrázku po špatnému uhadnutí pisména
function updateHangmanImage() {

  // Vytvoří SVG
  const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
  svg.setAttribute('width', '400');
  svg.setAttribute('height', '400');

  // Vytvoří line pro vertikal šibenice
  const sibeniceVert = document.createElementNS('http://www.w3.org/2000/svg', 'line');
  sibeniceVert.setAttribute('x1', '50'); // Координата X початку тіла
  sibeniceVert.setAttribute('y1', '0'); // Координата Y початку тіла
  sibeniceVert.setAttribute('x2', '50'); // Координата X кінця тіла
  sibeniceVert.setAttribute('y2', '400'); // Координата Y кінця тіла
  sibeniceVert.setAttribute('stroke', 'black'); // Колір контуру тіла
  sibeniceVert.setAttribute('stroke-width', '4'); // Ширина контуру тіла

  // Vytvoří line pro horizontal šibenice
  const sibeniceHor = document.createElementNS('http://www.w3.org/2000/svg', 'line');
  sibeniceHor.setAttribute('x1', '50'); // Координата X початку тіла
  sibeniceHor.setAttribute('y1', '0'); // Координата Y початку тіла
  sibeniceHor.setAttribute('x2', '200'); // Координата X кінця тіла
  sibeniceHor.setAttribute('y2', '0'); // Координата Y кінця тіла
  sibeniceHor.setAttribute('stroke', 'black'); // Колір контуру тіла
  sibeniceHor.setAttribute('stroke-width', '4'); // Ширина контуру тіла

  // Vytvoří line pro druhou vertikal šibenice
  const sibeniceVert2 = document.createElementNS('http://www.w3.org/2000/svg', 'line');
  sibeniceVert2.setAttribute('x1', '200'); // Координата X початку тіла
  sibeniceVert2.setAttribute('y1', '0'); // Координата Y початку тіла
  sibeniceVert2.setAttribute('x2', '200'); // Координата X кінця тіла
  sibeniceVert2.setAttribute('y2', '50'); // Координата Y кінця тіла
  sibeniceVert2.setAttribute('stroke', 'black'); // Колір контуру тіла
  sibeniceVert2.setAttribute('stroke-width', '4'); // Ширина контуру тіла

  // Vytvoří circle pro hlavu
  const head = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
  head.setAttribute('cx', '200'); // Координата X центру голови
  head.setAttribute('cy', '100'); // Координата Y центру голови
  head.setAttribute('r', '50'); // Радіус голови
  head.setAttribute('fill', 'none'); // Немає заповнення голови
  head.setAttribute('stroke', 'black'); // Колір контуру голови
  head.setAttribute('stroke-width', '4'); // Ширина контуру голови

  // Vytvoří line pro tělo
  const body = document.createElementNS('http://www.w3.org/2000/svg', 'line');
  body.setAttribute('x1', '200'); // Координата X початку тіла
  body.setAttribute('y1', '150'); // Координата Y початку тіла
  body.setAttribute('x2', '200'); // Координата X кінця тіла
  body.setAttribute('y2', '300'); // Координата Y кінця тіла
  body.setAttribute('stroke', 'black'); // Колір контуру тіла
  body.setAttribute('stroke-width', '4'); // Ширина контуру тіла

  // Vytvoří line pro ruce
  const leftArm = document.createElementNS('http://www.w3.org/2000/svg', 'line');
  leftArm.setAttribute('x1', '200'); // Координата X початку лівої руки
  leftArm.setAttribute('y1', '180'); // Координата Y початку лівої руки
  leftArm.setAttribute('x2', '150'); // Координата X кінця лівої руки
  leftArm.setAttribute('y2', '250'); // Координата Y кінця лівої руки
  leftArm.setAttribute('stroke', 'black'); // Колір контуру лівої руки
  leftArm.setAttribute('stroke-width', '4'); // Ширина контуру лівої руки

  const rightArm = document.createElementNS('http://www.w3.org/2000/svg', 'line');
  rightArm.setAttribute('x1', '200'); // Координата X початку правої руки
  rightArm.setAttribute('y1', '180'); // Координата Y початку правої руки
  rightArm.setAttribute('x2', '250'); // Координата X кінця правої руки
  rightArm.setAttribute('y2', '250'); // Координата Y кінця правої руки
  rightArm.setAttribute('stroke', 'black'); // Колір контуру правої руки
  rightArm.setAttribute('stroke-width', '4'); // Ширина контуру правої руки

  // Vytvoří line pro nohy
  const leftLeg = document.createElementNS('http://www.w3.org/2000/svg', 'line');
  leftLeg.setAttribute('x1', '200'); // Координата X початку лівої ноги
  leftLeg.setAttribute('y1', '300'); // Координата Y початку лівої ноги
  leftLeg.setAttribute('x2', '150'); // Координата X кінця лівої ноги
  leftLeg.setAttribute('y2', '380'); // Координата Y кінця лівої ноги
  leftLeg.setAttribute('stroke', 'black'); // Колір контуру лівої ноги
  leftLeg.setAttribute('stroke-width', '4'); // Ширина контуру лівої ноги

  const rightLeg = document.createElementNS('http://www.w3.org/2000/svg', 'line');
  rightLeg.setAttribute('x1', '200'); // Координата X початку правої ноги
  rightLeg.setAttribute('y1', '300'); // Координата Y початку правої ноги
  rightLeg.setAttribute('x2', '250'); // Координата X кінця правої ноги
  rightLeg.setAttribute('y2', '380'); // Координата Y кінця правої ноги
  rightLeg.setAttribute('stroke', 'black'); // Колір контуру правої ноги
  rightLeg.setAttribute('stroke-width', '4'); // Ширина контуру правої ноги

  if (wrongAttempts==0) {
    const svg1 = document.getElementById('hangman-svg');
    while (svg1.firstChild) { //Smaže vše co je momentalně vykreslene
      svg1.removeChild(svg1.firstChild);
    }

    //Přida aktuální elementy do SVG
    svg.appendChild(sibeniceVert);
    svg.appendChild(sibeniceHor);
    svg.appendChild(sibeniceVert2);
  }

  if (wrongAttempts==1) {
    const svg1 = document.getElementById('hangman-svg');
    while (svg1.firstChild) { //Smaže vše co je momentalně vykreslene
      svg1.removeChild(svg1.firstChild);
    }

    svg.appendChild(sibeniceVert);
    svg.appendChild(sibeniceHor);
    svg.appendChild(sibeniceVert2);
    svg.appendChild(head);
  }

  if (wrongAttempts==2) {
    const svg1 = document.getElementById('hangman-svg');
    while (svg1.firstChild) { //Smaže vše co je momentalně vykreslene
      svg1.removeChild(svg1.firstChild);
    }

    svg.appendChild(sibeniceVert);
    svg.appendChild(sibeniceHor);
    svg.appendChild(sibeniceVert2);
    svg.appendChild(head);
    svg.appendChild(body);
  }

  if (wrongAttempts==3) {
    const svg1 = document.getElementById('hangman-svg');
    while (svg1.firstChild) { //Smaže vše co je momentalně vykreslene
      svg1.removeChild(svg1.firstChild);
    }

    svg.appendChild(sibeniceVert);
    svg.appendChild(sibeniceHor);
    svg.appendChild(sibeniceVert2);
    svg.appendChild(head);
    svg.appendChild(body);
    svg.appendChild(leftArm);
  }

  if (wrongAttempts==4) {
    const svg1 = document.getElementById('hangman-svg');
    while (svg1.firstChild) { //Smaže vše co je momentalně vykreslene
      svg1.removeChild(svg1.firstChild);
    }

    svg.appendChild(sibeniceVert);
    svg.appendChild(sibeniceHor);
    svg.appendChild(sibeniceVert2);
    svg.appendChild(head);
    svg.appendChild(body);
    svg.appendChild(leftArm);
    svg.appendChild(rightArm);
  }

  if (wrongAttempts==5) {
    const svg1 = document.getElementById('hangman-svg');
    while (svg1.firstChild) { //Smaže vše co je momentalně vykreslene
      svg1.removeChild(svg1.firstChild);
    }

    svg.appendChild(sibeniceVert);
    svg.appendChild(sibeniceHor);
    svg.appendChild(sibeniceVert2);
    svg.appendChild(head);
    svg.appendChild(body);
    svg.appendChild(leftArm);
    svg.appendChild(rightArm);
    svg.appendChild(leftLeg);
  }

  if (wrongAttempts==6) {
    const svg1 = document.getElementById('hangman-svg');
    while (svg1.firstChild) { //Smaže vše co je momentalně vykreslene
      svg1.removeChild(svg1.firstChild);
    }

    svg.appendChild(sibeniceVert);
    svg.appendChild(sibeniceHor);
    svg.appendChild(sibeniceVert2);
    svg.appendChild(head);
    svg.appendChild(body);
    svg.appendChild(leftArm);
    svg.appendChild(rightArm);
    svg.appendChild(leftLeg);
    svg.appendChild(rightLeg);
  }

  //Přidává SVG do kontejneru
  const container = document.getElementById('hangman-svg');
  container.appendChild(svg);
}

// Funkce, která kontroluje, jestli slovo už není uhádnuté
function isWordGuessed() {
  for (let letter of randomWord) {
    if (!guessedLetters.includes(letter)) {
      return false;
    }
  }
  return true;
}

// Funkce pro inicializaci hry
function initializeGame() {
  chooseRandomWord();
  attempts = 0;
  wrongAttempts = 0;
  guessedLetters = [];
  updateGameDisplay();
  updateHangmanImage();
  restartButtonElement.style.display = 'none';
}

// Funkce pro zpracování zadaného pisména
function handleGuess() {
  const guess = guessInputElement.value.toLowerCase();
  guessInputElement.value = '';

//Validace vstupu
  if (guess.length !== 1 || "1234567890".indexOf(guess) != -1) {
    alert('Prosím, zadejte jedno pismeno.');
    return;
  }

  if (guessedLetters.includes(guess)) {
    alert('Už jste použil toto pismeno. Zkuste prosím znovu.');
    return;
  }

  attempts++;
  guessedLetters.push(guess);

  if (!randomWord.includes(guess)) {
    wrongAttempts++;
    updateHangmanImage();
    var audio = new Audio();
    audio.preload = 'auto';
    audio.src = 'fail.mp3';
    audio.play();
  }

  updateGameDisplay();

  if (isWordGuessed()) {
    var audio = new Audio();
    audio.preload = 'auto';
    audio.src = 'congratulations.mp3';
    audio.play();
    alert('Gratulují! Slovo jste uhadl! Je to '+(randomWord));
    restartButtonElement.style.display = 'block';
    guessButtonElement.disabled = true;
  } else if (wrongAttempts === 6) {
    var audio = new Audio();
    audio.preload = 'auto';
    audio.src = 'failed.mp3';
    audio.play();
    alert('Game over. Máte 6 špatných pokusů :( Slovo bylo '+(randomWord));
    restartButtonElement.style.display = 'block';
    guessButtonElement.disabled = true;
  }
}

// Funkce pro stisknuti tlačitka "Restart"
function handleRestartGame() {
    const svg1 = document.getElementById('hangman-svg');
    //Smaže vše co je momentalně vykreslene
    if (svg1) {
      while (svg1.firstChild) {
        svg1.removeChild(svg1.firstChild);
    }
  }
  initializeGame();
  restartButtonElement.style.display = 'none';
  guessButtonElement.disabled = false;
}

//Validace delky vstupu
function validateLength() {
   var inputfield = document.getElementById('guess-input');
   if (inputfield.value.length > 1) inputfield.value = inputfield.value.slice(-1);
}

// Event "Zadat"
guessButtonElement.addEventListener('click', handleGuess);

//Aby se dalo zadat pismenu enterem, nejen tlacitkem
guessInputElement.addEventListener("keypress", function(event) {
  if (event.key === "Enter") {
    event.preventDefault();
    document.getElementById("guess-button").click();
  }
});

// Event "Restart"
restartButtonElement.addEventListener('click', handleRestartGame);

// Inicializace hry při načtení stránky
initializeGame();
